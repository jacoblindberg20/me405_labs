'''
@file Lab3_Main.py
@brief Main file that runs onboard the Nucleo Microcontroller
@details This program lives onboard the microcontroller. It samples an analog signal
        once the user presses the user defined button on pin PC13. This analog signal
        is run through an Analog to Digital Converter that takes the digital values
        and populates an array based on the sample time defined by the user. The data
        array is then sent over the serial port to the local machine to be plotted.
@author Jacob Lindberg
@date January 26, 2021
'''
## STEPS FOR THIS LAB ##
# Import All Modules
import array
import pyb
import micropython

## Executive Button
Exec = False
# Allocate 200 bytes for the buffer
micropython.alloc_emergency_exception_buf(200)
## Create UART Object Tied to Serial 2
myuart = pyb.UART(2)
## Pin for the Analog Input
Pin1 = pyb.Pin.board.PC0 # Pin A5
## Small Buffer Array for Digital Conversion Values from Analog Signal
buffy_small = array.array ('H', (0 for index in range (500)))
## ADC object tied to the pin that is the analog input
adc = pyb.ADC(Pin1) 
## Timer Period Object for Timer 2
t2 = pyb.Timer(6, freq = 200000)


# Create an external interrupt for the button
def count_isr(PC13): # Create ISR
    '''
    @brief Interrupt Service Routine Function
    @details This function serves as the main ISR that halts current code progress when
            the interrupt is triggered. In this case, the interrupt occurs on pin PC13, and
            is triggered by the falling edge of the clock signal.
    @param PC13 Pin that is assigned to the user defined button
    '''
    global Exec
    Exec = True
    pass

## External Interrupt Pin  
extint = pyb.ExtInt (pyb.Pin.board.PC13,   # Which pin
             pyb.ExtInt.IRQ_FALLING,       # Interrupt on rising edge
             pyb.Pin.PULL_NONE,              # Activate pullup resistor
             callback = count_isr) 

## Large Buffer Array for Digital Conversion Values from Analog Signal
buffy_large = array.array ('H', (0 for index in range (500)))
# ADC While Loop
while True:
    if Exec == True:
        if adc.read() >= 5:
             adc.read_timed(buffy_large, t2)
             break
    else:
        adc.read_timed(buffy_small, t2)
        
print("Transfering Data... ")       
for n in range(len(buffy_large)):
    #print('{:},{:}\n'.format(n, buffy_large[n]))
    myuart.write('{:},{:}\n'.format(n, buffy_large[n]))
    
myuart.write('D\n') 

myuart.close()
 


