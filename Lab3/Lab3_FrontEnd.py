'''
@file Lab3_FrontEnd.py
@brief This is the file that runs on the local machine to talk with the microcontroller
@details  This program will convert the sent over data from the NUCLEO and graph
          the corresponding relationship between the ADC counter and time. The data
          array will be sent over via UART serial communication, and then terminated
          once the program has stopped with the data collection.
@author Jacob Lindberg
@date January 29, 2021
'''

import matplotlib.pyplot as plt
import serial
import numpy as np 
## Serial Port
ser = serial.Serial(port='COM3', baudrate = 115273, timeout = 1)
## Command Array
cmd = ''
# Input while loop
while cmd != 'G':
    cmd = input("Press 'G' to start orogram: ")
    if cmd == 'G':
        break
    else:
        pass
## Empty Time Array    
t = []
## Empty Volts Array
volts = []
# While Loop for the Program Execution
print("Press User Button at Any Time: ")
# While loop for the data collection
while True:
    try:
        string = ser.readline().decode('ascii')
        if string == 'D\n':
            break
        else:
            data = string.strip().split(',')
            t.append(float(data[0])/1000)
            volts.append(float(data[1])*(3.3/4095))
            
    except KeyboardInterrupt:
        break
    except:
        pass
    
print("The program has terminated")
## PLot the Graph
plt.plot(t, volts, 'b')
plt.ylabel('Volts(V)')
plt.xlabel('Time(s)')
plt.title('Voltage vs. Time')
plt.axis([0, 0.5, 0, 3.3])

my_array = np.asarray([t, volts])
np.savetxt('my_array.csv', my_array, delimiter=',')
        
    
ser.close()