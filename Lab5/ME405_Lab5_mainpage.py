'''
@page page_ME405_Lab5 ME 405: Rotating Platform Dynamics

Below is the dynamics for the ME 405 rotating platform.
@image html ME405_Platform.jpg width=700px height=600px

Platform & Ball Kinematics:
@image html Kinematics_Ball.jpg width=800px height=1035px
@image html Kinematics_Q.jpg width=800px height=1035px

Platform & Ball Kinetics:
@image html MomentCalcs1.jpg width=800px height=1035px
@image html MomentCalcs2.jpg width=800px height=1035px
@image html MomentCalcs3.jpg width=800px height=1035px
@image html MomentCalcs4.jpg width=800px height=1035px
@image html MomentCalcs5.jpg width=800px height=1035px

@authors Dakota Baker, Jacob Lindberg

@date February 15, 2021
'''