'''
@file Lab2_ThinkFast.py
@brief MicroPython Event Respose Game with LED testing Reaction Time
@details This program serves to support the Lab0x02 requirements in a functional format.
        This program will utilize what is known as an Interrupt Service Routine, that
        is triggered when the user pressed the button onboard the Nucleo Microcontroller.
        Upon trigger, the ISR will cascade into the timing loop to which a reaction time
        is calculated then stored within a global data array.
@author Jacob Lindberg
@date January 23, 2021
'''

import pyb
import micropython
import utime
import random
## Random delay list
random_delay = int(random.choice([2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3.0])*1000)
# Allocate 200 bytes for the buffer
micropython.alloc_emergency_exception_buf(200)
## Assign real LED Pin
myLED = pyb.Pin(pyb.Pin.cpu.A5,pyb.Pin.OUT_PP)
## Assign Timer to timer 2 with period and prescaler
t1 = pyb.Timer(2, prescaler = 0, period = 0X3FFFFFFF)
## Start time for program
start_time = 0
## End time for program
end_time = 0
## Assign initial delta value for timer overflow
delta = 0
## Binary execution variable used for ISR querry
Exec = False

print('In 10 seconds, the program will blink the LED 3 times and then one last time \n'
      'for a much longer duration before the program begins. \n'
      'Once the LED turns off after this long pause, press the blue user button \n'
      'after the next blink to guage reaction time in milliseconds \n'
      'To terminate the program and average total response time, press Control-C \n')

pyb.delay(10000)
myLED.high()
pyb.delay(200)
myLED.low()
pyb.delay(200)
myLED.high()
pyb.delay(200)
myLED.low()
pyb.delay(200)
myLED.high()
pyb.delay(200)
myLED.low()
pyb.delay(200)
myLED.high()
pyb.delay(2000)
myLED.low()
pyb.delay(2000)

def calc_time(start, end):
    '''
    @brief Computes the reaction time based on timer ticks
    @details This function passes in the start tick from utime module when the LED is high
            and the tick stamp when the user triggers the button. The difference in ticks
            is passed through an algorithm that computes the reaction time based on the 
            prescaler, period, and MCU clock speed, then returns that variable.
    @param start Current tick stamp that the timer reads when led first turns on
    @param end Tick stamp associated when ISR is triggered
    @return Time, in milliseconds, as an integer value
    '''
    if start <= end:
        delta = end - start
    elif start > end:
        delta = (1073741823 - start) + end
    else:
        pass
    time = (delta*(.218/16777215)) * 1000# divide by clock requency to get ms
    
    return int(time)

def count_isr(PC13): # Create ISR
    '''
    @brief Interrupt Service Routine Function
    @details This function serves as the main ISR that halts current code progress when
            the interrupt is triggered. In this case, the interrupt occurs on pin PC13, and
            is triggered by the falling edge of the clock signal.
    @param PC13 Pin that is assigned to the user defined button
    '''
    global end_time, Exec
    end_time = t1.counter()
    Exec = True
    pass

## External Interrupt Pin  
extint = pyb.ExtInt (pyb.Pin.board.PC13,   # Which pin
             pyb.ExtInt.IRQ_FALLING,       # Interrupt on rising edge
             pyb.Pin.PULL_UP,              # Activate pullup resistor
             callback = count_isr) 
## Current time stamp
time_current = utime.ticks_ms()
## Empty Reaction Time List
reac_array = []
# Main While Loop For Code
while True:
    try:
        pyb.delay(random_delay) # in microseconds
        Exec = False
        myLED.high()         
        time_begin = utime.ticks_ms()
        time_end = utime.ticks_ms() + 1000
        start_time = t1.counter()
        time_current = utime.ticks_ms()
        while time_current in range(time_begin, time_end):
            if Exec == True:
                reac_array.append(calc_time(start_time, end_time))
                print("Reaction Time: " + str(calc_time(start_time, end_time)) + "  ms.")
                break
            else:
                pass
            time_current = utime.ticks_ms()
        if Exec == False:
            print("No Interuppt Detected or Error, Please press Button After LED Turns On!")
        myLED.low()
        Exec = False
    except KeyboardInterrupt:
        avg = sum(reac_array)/(len(reac_array))
        print('Program has been terminated. Your average reaction time is: \n' 
              + str(avg) + (' milliseconds!'))
        break
    

        