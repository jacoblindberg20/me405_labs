'''
@page page_Lab6_SystemModeling Lab6: Simulink System Modeling

@details This lab was designed to implement the mathematical model and computational results
        determined in Lab 5 to design a full closed loop feedback system to simulate a steady state response
        to get the platform and ball back to the initial conditions established as equillibrium. Graphs were
        generated to further enhance our understanding of the behavior of our state-space variables
        (x, theta, x_der, theta_der) to visibly see the response of the system based on certain initial 
        parameters. Proportional gain coefficients were then added to the system to simulate a closed-loop 
        second order response. 
        
        

Below is the view of the Simulink Model that was used for our closed loop feedback system
@image html Lab6_SimulinkModel.png width=1035px height=800px


@section Part 3a: All initial conditions set to 0

@image html 3a_x.png width=800px height=1035px
Fig. 1a: Horizontal Distance of Ball(cm) vs. Time(s)

@image html 3a_theta.png width=800px height=1035px
Fig. 1b: Angular Distance(deg) vs. Time(s)

@image html 3a_x_der.png width=800px height=1035px
Fig. 1c: Ball Velocity (cm/s) vs. Time(s)

@image html 3a_theta_der.png width=800px height=1035px
Fig. 1d: Platform Angular Velocity (deg/s) vs. Time(s)

@section Part 3b: Ball offset by distance of 5cm from center; all other conditions at 0

@image html 3b_x.png width=800px height=1035px
Fig. 2a: Horizontal Distance of Ball(cm) vs. Time(s)

@image html 3b_theta.png width=800px height=1035px
Fig. 2b: Angular Distance(deg) vs. Time(s)

@image html 3b_x_der.png width=800px height=1035px
Fig. 2c: Ball Velocity (cm/s) vs. Time(s)

@image html 3b_theta_der.png width=800px height=1035px
Fig. 2d: Platform Angular Velocity (deg/s) vs. Time(s)

@section Part 3c: Platform tilted at initial angle of 5 deg; all other conditions at 0

@image html 3c_x.png width=800px height=1035px
Fig. 3a: Horizontal Distance of Ball(cm) vs. Time(s)

@image html 3c_theta.png width=800px height=1035px
Fig. 3b: Angular Distance(deg) vs. Time(s)

@image html 3c_x_der.png width=800px height=1035px
Fig. 3c: Ball Velocity (cm/s) vs. Time(s)

@image html 3c_theta_der.png width=800px height=1035px
Fig. 3d: Platform Angular Velocity (deg/s) vs. Time(s)

@section Part 3d: Torque impulse of 1 mNm; all other conditions at 0

@image html 3d_x.png width=800px height=1035px
Fig. 4a: Horizontal Distance of Ball(cm) vs. Time(s)

@image html 3d_theta.png width=800px height=1035px
Fig. 4b: Angular Distance(deg) vs. Time(s)

@image html 3d_x_der.png width=800px height=1035px
Fig. 4c: Ball Velocity (cm/s) vs. Time(s)

@image html 3d_theta_der.png width=800px height=1035px
Fig. 4d: Platform Angular Velocity (deg/s) vs. Time(s)

@section Part 4: ClosedLoop Feedback System with Gain Coefficients [x=-.2 theta=-.3 x_der=-.05 theta_der=-.02]

@image html 4_x.png width=800px height=1035px
Fig. 5a: Horizontal Distance of Ball(cm) vs. Time(s)

@image html 4_theta.png width=800px height=1035px
Fig. 5b: Angular Distance(deg) vs. Time(s)

@image html 4_x_der.png width=800px height=1035px
Fig. 5c: Ball Velocity (cm/s) vs. Time(s)

@image html 4_theta_der.png width=800px height=1035px
Fig. 5d: Platform Angular Velocity (deg/s) vs. Time(s)

@author Jacob Lindberg

@date February 21, 2021

'''
