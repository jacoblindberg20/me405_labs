'''
@file Lab7_FTPDriver.py
@brief Main file used to test capacitive sensor touch panel
@details This class serves to satisfy the driver that is associated with the touch
        screen resistive sensor that will be able to identify the location of the ball 
        along the x and y coordinates, as well as be able to specify their respective
        vector velocities associated with the movement of the ball ontop the platform.
        Pins are assigned to all three x,y,z axis networks so that each resistive divider
        is energized in a way to produce values that can be turned into their respective
        fractional values based on the length and width of the platform.
@author Jacob Lindberg
@date Feburary 26, 2021
'''

######## PIN OUTS ########
# Pin1 (PA0) =====> y+   bottom right
# Pin2 (PA1) =====> y-   top right
# Pin3 (PA6) =====> x+   bottom left
# Pin4 (PA7) =====> x-   top left
from pyb import udelay
from pyb import Pin
from pyb import ADC
import utime

pin1 = Pin(Pin.cpu.A0)
pin2 = Pin(Pin.cpu.A1) 
pin3 = Pin(Pin.cpu.A6)
pin4 = Pin(Pin.cpu.A7)


class FTP:
    '''
    @brief Create a driver class for the Touch Panel that will return distance the ball is from center
    @param pin1 Pin PA0 on Nucleo; assigned to y+ terminal
    @param pin2 Pin PA1 on Nucleo; assigned to y- terminal
    @param pin3 Pin PA6 on Nucleo; assigned to x+ terminal
    @param pin4 Pin PA7 on Nucleo; assigned to x- terminal
    '''
    
    
    def __init__(self, width, length, Y0, X0):
        
        '''
        @brief Create a FTP constructor class that contains functionality for FTP driver
        '''

        ## Width of the touch plate in mm
        self.width = width
        ## Length of the plate in mm
        self.length = length
        ## Center coordinate of the X axis in mm
        self.X0 = X0
        ## Center coordinate of the Y axis in mm
        self.Y0 = Y0
        
        
    def x_scan(self):
        
        '''
        @brief x_scan method that scans the analog read pin1 and converts to x distance
        @return x distance, in mm, that the ball is from the center of the platform
        '''
        pin3.init(mode = pin3.OUT_PP, value = 1)
        pin4.init(mode = pin4.OUT_PP, value = 0)
        pin2.init(mode = pin2.IN)
        pin1.init(mode = pin1.IN)
        ADC_xm = ADC(pin1)
        val_x = ADC_xm.read()/4095
        udelay(3)
        xdist = round((val_x*self.length) - self.X0, 2)
        return xdist

    def y_scan(self):
        
        '''
        @brief y_scan method that scans the analog read pin3 and converts to y distance
        @return y distance, in mm, that the ball is from the center of the platform
        '''
        pin1.init(mode = pin1.OUT_PP, value = 1)
        pin2.init(mode = pin2.OUT_PP, value = 0)
        pin4.init(mode = pin4.IN)
        pin3.init(mode = pin3.IN)
        ADC_ym = ADC(pin3)
        val_y = ADC_ym.read()/4095
        udelay(3)
        ydist = round((((val_y)*self.width) - self.Y0)*-1, 2)
        return ydist
    
    def z_scan(self):
        
        '''
        @brief z_scan method that scans the analog read pin and returns boolean if panel senses weight
        @return boolean True or False associated with whether or not something is in contact with panel
        '''
        pin1.init(mode = pin1.OUT_PP, value = 1)
        pin4.init(mode = pin4.OUT_PP, value = 0)
        #pin2 = pyb.Pin(pyb.Pin.cpu.A1)
        ADC_zm = ADC(pin2)
        val_z = ADC_zm.read()
        udelay(3)
        if val_z > 4000:
            return False
        else:
            return True
        
    def xyztotuple(self):
        
        '''
        @brief method used to take all return values from axis methods and populate a tuple with them
        @return tuple containing return values from each method as well as the time,
                in microseconds, that it takes to complete one task
        '''
        start = utime.ticks_us()
        xscan = self.x_scan()
        yscan = self.y_scan()
        zscan = self.z_scan()
        end = utime.ticks_us()
        time = end - start
        return (xscan, yscan, zscan, time)
    
    
###############################################################

    
## Define the task for testing
task1 = FTP(100, 177, 50, 88)

while True:
    print(task1.xyztotuple())
    
    
        
    

    
    
    

    
    
