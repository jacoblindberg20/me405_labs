'''
@file Lab1_VendotronFSM.py 
@image html VendotronDiagram.PNG
@brief Vending Machine Finite State Machine
@details This program serves to support the Lab0x01 requirements in a functional format.
        This program will vend a desired drink based on how much money is input into the machine
        as well as allow for the selection of amongst 4 drinks and deliver exact change. The program
        allows for the user to eject their inputed money at any time during the program before the
        vending machine is told to dispense a drink.
@author Jacob Lindberg
@date January 12, 2021
'''

import math # import the math module
import keyboard # import the keyboard module

## State variable set to 0
state = 0 # First state variable
## Cost of the Cuke
Cuke = 1.00 # Cuke Price
## Cost of the Popsi
Popsi = 1.20 # Popsi Price
## Cost of the Spryte
Spryte = 0.85 # Spryte Price
## Cost of the Dr. Pupper
DrPupper = 1.10 # Dr. Pupper Price
## Empty denomination list
Denom = [0,0,0,0,0,0,0,0] # Payment list
## Set global pushed_key variable to None
pushed_key = None


def getChange(price, payment):
    '''
    @brief Computes change from difference of price and payment methods
    @details This function utilizes a passed in price of the user's desired purchase, then takes the
            tuple that represents the denominations of possible bills and coins to compute the difference
            in the total price and payment values to eject exact change.
    @param price The total cost of item being purchased, in USD to two decimal places
    @param payment Tuple variable with 8 indecies, one for pennies, nickels, dimes, quarters, ones, fives, tens, twenties
    @return change Tuple variable containing the denominations for the exact change
    '''
    pennies = payment[0]*.01
    nickels = payment[1]*.05
    dimes = payment[2]*.10
    quarters = payment[3]*.25
    singles = payment[4]*1.00
    fives = payment[5]*5.00
    tens = payment[6]*10.00
    twenties = payment[7]*20.00
    change_0 = 0
    change_1 = 0
    change_2 = 0
    change_3 = 0
    change_4 = 0
    change_5 = 0
    change_6 = 0
    change_7 = 0
    
    total = pennies+nickels+dimes+quarters+singles+fives+tens+twenties
    if total >= price:
        diff = (total - price)
        
        while True:
            if diff/20 >= 1: # change in twenties
                change_7 = math.floor(diff/20)
                diff = diff - 20*change_7
            elif diff/10 >= 1: # change in tens
                change_6 = math.floor(diff/10)
                diff = diff - 10*change_6
            elif diff/5 >= 1: # change in fives
                change_5 = math.floor(diff/5)
                diff = diff - 5*change_5
            elif diff/1 >= 1: # change in ones
                change_4 = math.floor(diff/1)
                diff = diff - 1*change_4
            elif diff/.25 >= 1: # change in quarters
                change_3 = math.floor(diff/.25)
                diff = diff - .25*change_3
            elif diff/.10 >= 1: # change in dimes
                change_2 = math.floor(diff/.10)
                diff = diff - .10*change_2
            elif diff/.05 >= 1: # change in nickels
                change_1 = math.floor(diff/.05)
                diff = diff - .05*change_1
            elif diff/.01 >= 1: # change in pennies
                change_0 = math.floor(diff/.01)
                diff = diff - .01*change_0
                break  
        change = (change_0, change_1, change_2, change_3, change_4, change_5, change_6, change_7)
    else:
        print("You have insufficient funds! Please insert " + str(price - total) + " more dollars.")
        print("")
        global state
        state = 1
        change = 0
    return change

def printWelcome():
    '''
    @brief Prints welcome message
    @details This function displays the opening Vendotron message upon startup
    '''
    print('Hello, welcome to Vendotron!')
    pass
def drinkSelect():
    '''
    @brief Gives the user the drink selection choices
    @details Prints display message showing the types of drinks that the Vendotron
            machine offers for purchase
    '''
    print('Please make a selection: "C" = Cuke, "P" = Popsi, "D" = Dr. Pupper, "S" = Spryte')
    pass
def totalPayment(payment):
    '''
    @brief Calculates the total currency from list
    @details This function takes the individual indicies within the tuple or list that is 
            passed into the function and computes the sum of the denominations to compute total
            amount of money in USD.
    @param payment List or Tuple thats passed in containing coin and bill denominations
    @return total The total of dollars as a joint sum
    '''
    pennies = payment[0]*.01
    nickels = payment[1]*.05
    dimes = payment[2]*.10
    quarters = payment[3]*.25
    singles = payment[4]*1.00
    fives = payment[5]*5.00
    tens = payment[6]*10.00
    twenties = payment[7]*20.00
    
    total = pennies+nickels+dimes+quarters+singles+fives+tens+twenties
    return round(total, 2)

def onKeyPress(thing):
    '''
    @brief Builds the global keyboard variable
    @details This function is used to store the most recent pressed input keyboard value
    @param thing Can be anything that the user wants this to be
    '''
    global pushed_key
    pushed_key = thing.name
    
keyboard.on_press(onKeyPress) # Call the keyboard pressed function 

while True:
    if state == 0: # Initialization
        Denom = [0,0,0,0,0,0,0,0] # Reset the denom indicies to 0
        printWelcome()
        state = 1        
    elif state == 1: # Prompt money
        print("Please insert your coins and bills by pressing on the correct numerical values from 0-7. When done, press 'X'. ")
        while True:
            try:
                if pushed_key:
                    if pushed_key == '0':
                        Denom[0] += 1
                        print("Penny Inserted. Current Balance: $" + str(totalPayment(Denom)) + " Press 'X' to select drink")
                        state = 2
                    elif pushed_key == '1':
                        Denom[1] += 1
                        print("Nickel Inserted. Current Balance: $" + str(totalPayment(Denom)) + " Press 'X' to select drink")
                        state = 2
                    elif pushed_key == '2':
                        Denom[2] += 1
                        print("Dime Inserted. Current Balance: $" + str(totalPayment(Denom)) + " Press 'X' to select drink")
                        state = 2
                    elif pushed_key == '3':
                        Denom[3] += 1
                        print("Quarter Inserted. Current Balance: $" + str(totalPayment(Denom)) + " Press 'X' to select drink")
                        state = 2
                    elif pushed_key == '4':
                        Denom[4] += 1
                        print("One Dollar Inserted. Current Balance: $" + str(totalPayment(Denom)) + " Press 'X' to select drink")
                        state = 2
                    elif pushed_key == '5':
                        Denom[5] += 1
                        print("Five Dollars Inserted. Current Balance: $" + str(totalPayment(Denom)) + " Press 'X' to select drink")
                        state = 2
                    elif pushed_key == '6':
                        Denom[6] += 1
                        print("Ten Dollars Inserted. Current Balance: $" + str(totalPayment(Denom)) + " Press 'X' to select drink")
                        state = 2
                    elif pushed_key == '7':
                        Denom[7] += 1
                        print("Twenty Dollars Inserted. Current Balance: $" + str(totalPayment(Denom)) + " Press 'X' to select drink")
                        state = 2
                    elif pushed_key == 'X':
                        pushed_key = None
                        break
                    elif pushed_key == 'E':
                        print(str(totalPayment(Denom)) + (' dollars has been ejected'))
                        print("")
                        state = 0
                        pushed_key = None
                        break
                    pushed_key = None
            except KeyboardInterrupt:
                print("Enter a number 0-7.")
    elif state == 2: # Drink Selection
        print("The tender provided totals: $" + str(totalPayment(Denom)))
        drinkSelect()
        while True:
            if keyboard.is_pressed('C'):
                print('You have selected Cuke!')
                state = 3
                x = getChange(Cuke, Denom)
                break 
            elif keyboard.is_pressed('P'):
                print('You have selected Popsi!')
                state = 3
                x = getChange(Popsi, Denom)
                break
            elif keyboard.is_pressed('D'):
                print('You have selected Dr. Pupper')
                state = 3
                x = getChange(DrPupper, Denom)
                break
            elif keyboard.is_pressed('S'):
                print('You have selected Spryte')
                state = 3
                x = getChange(Spryte, Denom)
                break
            elif keyboard.is_pressed('E'):
                x = totalPayment(Denom)
                print(str(x) + ' dollars has been ejected. Current balance is 0')
                state = 0
                break
            else:
                pass
    elif state == 3: # Vend Drink
        print("Drink has been successfully vended! Your change is: $" + str(totalPayment(x)))
        print("")
        state = 0
    elif keyboard.is_pressed('E'): # Eject Change
        print(str(totalPayment(Denom)) + (' dollars has been ejected'))
        state = 0
        
        
        